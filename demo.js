let shop = document.getElementById('shop');

let shopItemsData = [{
    id: "agavaaba",
    name: "Casual Shirt",
    price: 45,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/ali-muhamad-hT1R6Z5pY5I-unsplash.jpg"
}, {
    id: "xcvbnnn",
    name: "Office Shirt",
    price: 100,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/nimble-made-_PFanxhwe4o-unsplash.jpg"
}, {
    id: "uhnnbbb",
    name: "T Shirt",
    price: 25,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/haryo-setyadi-acn5ERAeSb4-unsplash(1).jpg"
}, {
    id: "adffgcfggg",
    name: "Mens Suit",
    price: 300,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/chase-charaba-3JAOcgZ_ZXU-unsplash.jpg"
}]
let basket = JSON.parse(localStorage.getItem("data")) || [];

let generateShop = () => {
   return (shop.innerHTML = shopItemsData.map((x) =>{
    let {id, name, price, desc, img} = x;
    let search = basket.find((x)=>x.id ===id) || []
    return ` 
    <div class="items" id="product-id-${id}">
    <img width="220" height="200" src="${img}" alt="">
    <div class="details">
      <h3>${name}</h3>
      <p>${desc}</p>
      <div class="price-quantity">
          <h2>$ ${price}</h2>
          <div class="buttons">
              <i onclick = "decrement(${id})" class="bi bi-dash-lg"></i>
              <div class="quantity" id="${id}">
              ${search.item === undefined? 0: search.item}</div>
              <i onclick = "increment(${id})" class="bi bi-plus-lg"></i>
          </div>
      </div>
    </div>
 
  </div>
    
    
    `
   }).join(''))
}
generateShop();



let increment = (id) => {
    let selectedItem = id;
    let search = basket.find((x) => x.id === selectedItem.id)
    if (search === undefined) {
        basket.push({
            id: selectedItem.id,
            item: 1
        })
    } else{
        search.item += 1;
    }

// console.log(basket)
update(selectedItem.id);
localStorage.setItem("data",JSON.stringify(basket))


};
let decrement = (id) => {
    let selectedItem = id;
    let search = basket.find((x) => x.id === selectedItem.id);
    if (search === undefined)  return;
    else if (search.item === 0) return;
       else{
        search.item -= 1;
    }
    update(selectedItem.id);

    basket = basket.filter((x) => x.item !== 0);
// console.log(basket)
    localStorage.setItem("data",JSON.stringify(basket))

};

let update = (id) => {
let search = basket.find((x) => x.id === id)
document.getElementById(id).innerHTML = search.item;
calculation();
};

let calculation = () => {
let cartIcon = document.getElementById('cart-amount');
cartIcon.innerHTML = basket.map((x) => x.item ).reduce((x,y) => x + y, 0);
}
calculation();
