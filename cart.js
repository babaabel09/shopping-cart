let label = document.getElementById('label');
let shoppingCart = document.getElementById('shopping-cart');


let basket = JSON.parse(localStorage.getItem("data")) || [];

let shopItemsData = [{
    id: "agavaaba",
    name: "Casual Shirt",
    price: 45,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/ali-muhamad-hT1R6Z5pY5I-unsplash.jpg"
}, {
    id: "xcvbnnn",
    name: "Office Shirt",
    price: 100,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/nimble-made-_PFanxhwe4o-unsplash.jpg"
}, {
    id: "uhnnbbb",
    name: "T Shirt",
    price: 25,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/haryo-setyadi-acn5ERAeSb4-unsplash(1).jpg"
}, {
    id: "adffgcfggg",
    name: "Mens Suit",
    price: 300,
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing.",
    img: "images/chase-charaba-3JAOcgZ_ZXU-unsplash.jpg"
}]
let calculation = () => {
    let cartIcon = document.getElementById('cart-amount');
    cartIcon.innerHTML = basket.map((x) => x.item ).reduce((x,y) => x + y, 0);
    }
    calculation();
 let generateCartItem = () => {
    if (basket.length !==0) {
        return(shoppingCart.innerHTML = basket.map((x)=>{
            let {id, item} = x;
            let search = shopItemsData.find((y) => y.id === id) || [];
            return `
                 <div class="cart-item">
                 <img width="100" height="100" src=${search.img} alt="" />
                 <div class="details">
                 <div class="title-price-x">
                    <h4 class="title-price">
                      <p>${search.name}</p>
                      <p class="cart-item-price">$ ${search.price}</p>
                    
                    </h4>
                    <i class="bi bi-x-lg" onclick="removeItem(${id})"></i>
                 </div>
                 <div class="buttons">
                 <i onclick = "decrement(${id})" class="bi bi-dash-lg"></i>
                 <div class="quantity" id="${id}">${item}</div>
                 <i onclick = "increment(${id})" class="bi bi-plus-lg"></i>
                 </div>
        
                 <h3>$ ${item*search.price}</h3>
                </div>
                 
                 </div>
             
            `
        }).join(""));
    } else{
        shoppingCart.innerHTML = ``;
        label.innerHTML = `
        <h2>Cart is Empty</h2>
        <a href= "index.html">
        <button class="HomeBtn">Back to home</button>
        </a>
        
        `;
    }
 };
 generateCartItem ();
 let increment = (id) => {
    let selectedItem = id;
    let search = basket.find((x) => x.id === selectedItem.id)
    if (search === undefined) {
        basket.push({
            id: selectedItem.id,
            item: 1
        })
    } else{
        search.item += 1;
    }

    generateCartItem ();

update(selectedItem.id);
localStorage.setItem("data",JSON.stringify(basket))


};
let decrement = (id) => {
    let selectedItem = id;
    let search = basket.find((x) => x.id === selectedItem.id);
    if (search === undefined)  return;
    else if (search.item === 0) return;
       else{
        search.item -= 1;
    }
    update(selectedItem.id);

    basket = basket.filter((x) => x.item !== 0);
    generateCartItem ();

    localStorage.setItem("data",JSON.stringify(basket))

};

let update = (id) => {
let search = basket.find((x) => x.id === id)
document.getElementById(id).innerHTML = search.item;
calculation();
totalAmount();

};

let removeItem = (id) => {
let selectedItem = id;
basket = basket.filter((x)=>x.id !==selectedItem.id );
generateCartItem ();
totalAmount();

calculation();

localStorage.setItem("data",JSON.stringify(basket))

}
let clearCart = () => {
      basket = []
      generateCartItem ();
    calculation();
      
      localStorage.setItem("data",JSON.stringify(basket))
  
}


let totalAmount = ()=>{
    if (basket.length !== 0) {
let amount = basket.map((x) =>{
    let {item,id} = x;
    let search = shopItemsData.find((y) => y.id === id) || [];
    return (item*search.price)
}).reduce((x,y)=> x+y, 0)
    label.innerHTML = `
    <h2>Total Bill: $ ${amount}</h2>
    <button class="checkout">Checkout</button>
    <button class="removeAll" onclick="clearCart ()">Clear Cart</button>
    `
    }else return;
}
totalAmount();